# Phpcs

little project to use phpcs in every projects

## Getting Started

if you want to edit the phpcs you can change Config section into the Dockerfile.

## How to use 

just call for exemple

```
/usr/local/phpcs file.php
```

### Prerequisites

You need docker installed on your system.

```
docker --version
```

### Installing
You need to pull the blackmorth/phpcs image

```
docker pull blackmorth/phpcs
```

copy the phpcs into /usr/local/phpcs

```
cp phpcs /usr/local/phpcs
```


